function [ofs, gain, gain_average, rotM] = ellipsoid_fit (xyz)
  x = xyz(:,1);
  y = xyz(:,2);
  z = xyz(:,3);
  
  D = [x.*x, y.*y, z.*z, 2*x.*y, 2*x.*z, 2*y.*z, 2*x, 2*y, 2*z];
  v = (D'*D)\(D' * ones(length(x), 1));
  
  A = [v(1) v(4) v(5) v(7);
       v(4) v(2) v(6) v(8);
       v(5) v(6) v(3) v(9);
       v(7) v(8) v(9) -1 ];
       
  ofs = -A(1:3, 1:3) \ [v(7); v(8); v(9)];  % offset is center of ellipsoid
  Tmtx =  eye(4);
  Tmtx(4, 1:3) = ofs';
  AT = Tmtx*A*Tmtx'; % ellipsoid translated to (0, 0, 0)
  [rotM ev] = eig(AT(1:3, 1:3) / -AT(4,4)); % eigenvectors (rotation) and eigenvalues (gain)
  gain = sqrt(1./diag(ev)); % gain is radius of ellipsoid
  
  % rotation matrix refinement
  m = 0;
  rm = 0;
  cm = 0;
  for r=1:3, for c = 1:3,
    if abs(rotM(r, c)) > m, m=abs(rotM(r, c)); rm = r; cm = c; end;
  end; end;
  if rm ~= cm,
    t = rotM(:,cm); rotM(:,cm)=rotM(:,rm); rotM(:,rm)=t;
    t = gain(cm); gain(cm)=gain(rm); gain(rm) = t;
  end;
  
  switch rm, case 1, i=[2 3]; case 2, i = [1, 3]; case 3, i = [1 2]; end;
  m = 0; rm = 0; cm = 0;
  for r = 1:2, for c = 1:2,
    if abs(rotM(i(r), i(c)))>m, m=abs(rotM(i(r), i(c))); rm=i(r); cm=i(c); end;
  end; end;
  if rm ~= cm,
    t = rotM(:,cm); rotM(:,cm)=rotM(:,rm); rotM(:,rm)=t;
    t = gain(cm); gain(cm)=gain(rm); gain(rm)=t;
  end;
  
  if(rotM(1,1))<0, rotM(:,1)=-rotM(:,1); end;
  if(rotM(2,2))<0, rotM(:,2)=-rotM(:,2); end;
  if(rotM(3,3))<0, rotM(:,3)=-rotM(:,3); end;
  
  % Gain-Faktor auf durchschnittswert skalieren (damit wird in der Verarbeitung 
  % der Kalibrierung im Microcontroller aus einer Division eine Multiplikation)
  gain_average = mean(gain);
  gain(1) = gain_average / gain(1);
  gain(2) = gain_average / gain(2);
  gain(3) = gain_average / gain(3);
endfunction
