# Magnetometer-Kalibrierung

Einthält ein Octave-Skript zur Erzeugung von Kalibrationsdaten für die Verwendung in der IMU-Einheit (Modul Konzeption und Durchführung autonomer Missionen, Sommersemester 2020 - EAH-Jena).

Zum Kalibrieren der Messwerte wird eine Datei mit unkalibrierten Rohdaten geladen, per Ellipsoiden-fitting werden Korrekturwerte für Offset, Gain und Rotation erzeugt, die vom STM32-Microcontroller verarbeitet werden können.