% ELLIPSOID FITTING ZUR KALIBRATION VON MAGNETOMETER-MESSWERTEN
% Es werden Offsets, Gains und Rotationen berechnet, die vom STM32F103-Controller
% verwendet werden.
% Die unkalibrierten und kalibrierten Messwerte k�nnen grafisch nebeneinander dargestellt werden
% Quelle der Routine f�r Ellipsoiden-Fitting:
% "Ellipsoid or sphere fitting for sensor calibration", STMicroelectronics
% www.st.com/content/ccc/resource/technical/document/design_tip/group0/a2/98/f5/d4/9c/48/4a/d1/DM00286302/files/DM00286302.pdf/jcr:content/translations/en.DM00286302.pdf

plot3d = true;

% Unkalibrierte Messwerte einlesen
% Befehl zum Auslesen eines Dateipfades in Octave. (unterscheided sich von Matlab)
[file, path] = uigetfile;   
fullpath = fullfile(path, file);
% Separator der Messwerte ist hier ";", kann bei Bedarf ge�ndert werden
xyz = dlmread(fullpath, ';');   

% Ellipsoiden Fit durchf�hren    
[ofs, gain, gain_average, rotM] = ellipsoid_fit(xyz)

% Kalibrierte Datenpunkte berechnen
xc = xyz(:,1) - ofs(1);
yc = xyz(:,2) - ofs(2);
zc = xyz(:,3) - ofs(3);
xyzc = [xc, yc, zc] * rotM;
xc = xyzc(:,1) * gain(1);
yc = xyzc(:,2) * gain(2);
zc = xyzc(:,3) * gain(3);

% Ellipsoide aus nicht kalibrierten Rohdaten erzeugen
[xe, ye, ze] = ellipsoid(ofs(1), ofs(2), ofs(3), gain(1) * gain_average, gain(2) * gain_average, gain(3) * gain_average);
coords = [xe(:), ye(:), ze(:)];
coords * rotM;

% Ellipsoide aus kalibrierten Datenpunkten erzeugen
[xn, yn, zn] = ellipsoid(0, 0, 0, gain_average, gain_average, gain_average);

% Plotten in 2D-Plots
if (plot3d == false),
  % Nicht kalibrierte Datenpunkte plotten
  figure;
  subplot(2,2,1); hold on; plot(xyz(:,1), xyz(:,2), 'ro'); plot(coords(:,1), coords(:,2), 'kx');
  xlabel('X'); ylabel('Y'); axis equal; grid on;
  subplot(2,2,2); hold on; plot(xyz(:,3), xyz(:,2), 'go'); plot(coords(:,3), coords(:,2), 'kx');
  xlabel('Z'); ylabel('Y'); axis equal; grid on;
  subplot(2,2,3); hold on; plot(xyz(:,1), xyz(:,3), 'bo'); plot(coords(:,1), coords(:,3), 'kx');
  xlabel('X'), ylabel('Z'); axis equal; grid on;
  title('Non-Calibrated data');

  % kalibrierte Datenpunkte plotten
  figure;
  subplot(2,2,1); hold on; plot(xc, yc, 'ro'); plot(xn, yn, 'kx');
  xlabel('X'); ylabel('Y'); axis equal; grid on;
  subplot(2,2,2); hold on; plot(zc, yc, 'go'); plot(zn, yn, 'kx');
  xlabel('Z'); ylabel('Y'); axis equal; grid on;
  subplot(2,2,3); hold on; plot(xc, zc, 'bo'); plot(xn, zn, 'kx');
  xlabel('X'), ylabel('Z'); axis equal; grid on;
  title('Calibrated data');
  
% Plotten in 3D-Plots
else,
  % Nicht kalibrierte Datenpunkte plotten
  figure;
  hold on;
  plot3(xyz(:,1), xyz(:,2), xyz(:,3), 'o');
  plot3(coords(:,1), coords(:,2), coords(:,3), 'kx');
  xlabel('X'); ylabel('Y'); zlabel('Z'); axis equal; grid on;
  
  % kalibrierte Datenpunkte plotten
  figure;
  hold on;
  plot3(xc, yc, zc, 'o');
  plot3(xn, yn, zn, 'kx');
  xlabel('X'); ylabel('Y'); zlabel('Z'); axis equal; grid on;
end;
